import { IUser } from '../../../shared/interfaces/user';
import { Car } from './car';

export class User {
  id: string;
  email: string;
  nickname: string;
  avgCPM: number;
  totalRaceAmount: number;
  wonRaceAmount: number;
  cars: Array<Car>;
  balance: number;
  rating: number;

  constructor(user: IUser) {
    this.id = user._id;
    this.email = user.email;
    this.nickname = user.nickname;
    this.avgCPM = user.avgCPM;
    this.totalRaceAmount = user.totalRaceAmount;
    this.wonRaceAmount = user.wonRaceAmount;
    this.cars = user.cars.map(model => new Car({ model }));
    this.balance = user.balance;
    this.rating = user.rating;
  }
}