export enum AuthorizationType {
  None = 0,
  Optional = 1,
  Mandatory = 2
}

export interface APIManagerConfig {
  authorizationTokenFactory: () => string | null;
  showLoader: () => void;
  hideLoader: () => void;
  refreshTokenFactory: () => string | null;
}

export interface APIManagerFetchOptions {
  authorizationType?: AuthorizationType;
  isGlobalLoaderShown?: boolean;
}

const defaultFetchOptions: APIManagerFetchOptions = {
  authorizationType: AuthorizationType.None,
  isGlobalLoaderShown: true
};

export class APIManager {
  private config: APIManagerConfig;

  constructor(config: APIManagerConfig) {
    this.config = config;
  }

  public fetch(options: APIManagerFetchOptions | null, input?: string | Request, init?: RequestInit) {
    const mergedOptions = {
      ...defaultFetchOptions,
      ...options
    };

    if (this.config.showLoader && mergedOptions.isGlobalLoaderShown !== false) {
      this.config.showLoader();
    }

    const headers: HeadersInit = init
      ? init.headers
      : undefined;

    return this.includeAuthHeader(mergedOptions.authorizationType, headers)
      .then(updatedHeaders => {
        const updatedInit: RequestInit = {
          ...init,
          headers: updatedHeaders
        };

        return fetch(input, updatedInit);
      })
      .then(res => {
        if (this.config.showLoader && mergedOptions.isGlobalLoaderShown !== false) {
          this.config.hideLoader();
        }
        return res;
      })
      .catch(err => {
        if (this.config.showLoader && mergedOptions.isGlobalLoaderShown !== false) {
          this.config.hideLoader();
        }
        throw err;
      });
  }

  private includeAuthHeaderIfExists(headers: HeadersInit = {}): Promise<HeadersInit> {
    return new Promise(resolve => {
      const authorizationToken = this.config.authorizationTokenFactory();
      if (authorizationToken) {
        const updatedHeaders = {
          ...headers,
          'Authorization': `Bearer ${authorizationToken}`
        };
        resolve(updatedHeaders);
      } else {
        resolve(headers);
      }
    });
  }

  private includeAuthHeader(authType: AuthorizationType, headers: HeadersInit = {}): Promise<HeadersInit> {
    switch (authType) {
      case AuthorizationType.None:
        return Promise.resolve(headers);
      case AuthorizationType.Optional:
        return this.includeAuthHeaderIfExists(headers);
      case AuthorizationType.Mandatory:
        return new Promise((resolve, reject) => {
          this.includeAuthHeaderIfExists(headers)
            .then(headers => {
              if (headers['Authorization']) {
                resolve(headers)
              } else {
                reject();
              }
            });
        });
    }
  }
}