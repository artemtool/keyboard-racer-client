import './Dashboard.scss';
import * as React from 'react';
import { connect } from 'react-redux';
import { createRace } from '../../state/actions/race';
import { History } from 'history';
import * as routes from '../../constants/routes';
import CreateRaceForm from './CreateRaceForm';
import { fetchRaceHistoryPage } from '../../state/actions/raceHistory';
import { AppState } from './../../state/appState';
import { IRace } from '../../../../shared/interfaces/race';
import { fetchUserRatingPage } from '../../state/actions/userRating';
import { User } from './../../models/user';

interface Props {
  history: History;
  user: User;
  raceHistory: Array<Array<IRace>>;
  isRaceHistoryLoading: boolean;
  raceHistoryCurrentPage: number;
  raceHistoryPagesTotal: number;
  userRating: Array<Array<User>>;
  isUserRatingLoading: boolean;
  userRatingCurrentPage: number;
  userRatingPagesTotal: number;
  fetchRaceHistoryPage: (page: number, isInitialLoad: boolean) => Promise<void>;
  fetchUserRatingPage: (page: number, isInitialLoad: boolean) => Promise<void>;
  createRace: (membersNeeded: number) => Promise<void>;
}

class DashboardPage extends React.PureComponent<Props> {

  constructor(props: Props) {
    super(props);

    this.onRaceCreate = this.onRaceCreate.bind(this);
  }

  componentDidMount() {
    this.props.fetchRaceHistoryPage(1, true);
    this.props.fetchUserRatingPage(1, true);
  }

  public render() {
    return (
      <React.Fragment>
        <h2>Dashboard</h2>
        <div className="dashboard">
          <div className="dashboard__tiles-group dashboard__tiles-group--left">
            <div className="dashboard-tile">
              <div className="dashboard-tile__header">
                <h3>My info</h3>
              </div>
              Rating: {this.props.user.rating}<br />
              Balance: {this.props.user.balance}<br />
              Average CPM: {this.props.user.avgCPM}<br />
            </div>
            <div className="dashboard-tile dashboard-tile--top-offset">
              <div className="dashboard-tile__header">
                <h3>Create race</h3>
              </div>
              <CreateRaceForm onSubmit={this.onRaceCreate} />
            </div>
            <div className="dashboard-tile dashboard-tile--top-offset">
              <div className="dashboard-tile__header">
                <h3>Global Rating</h3>
              </div>
              {
                this.props.userRating[this.props.userRatingCurrentPage] && (
                  <React.Fragment>

                    <table>
                      <thead>
                        <tr>
                          <td>Nickname</td>
                          <td>Rating</td>
                          <td>Balance</td>
                        </tr>
                      </thead>
                      <tbody>
                        {
                          this.props.userRating[this.props.userRatingCurrentPage].map(user =>
                            <tr key={user.id}>
                              <td>{user.nickname}</td>
                              <td>{user.rating}</td>
                              <td>{user.balance}</td>
                            </tr>
                          )
                        }
                      </tbody>
                      <tfoot>
                        <tr>
                          <td colSpan={3}>Page: {this.props.userRatingCurrentPage} / {this.props.userRatingPagesTotal}</td>
                        </tr>
                      </tfoot>
                    </table>
                    <button
                      disabled={this.props.userRatingCurrentPage === 1}
                      onClick={() => this.props.fetchUserRatingPage(1, false)}
                    >&lt;&lt;</button>
                    <button
                      disabled={this.props.userRatingCurrentPage === 1}
                      onClick={() => this.props.fetchUserRatingPage(this.props.userRatingCurrentPage - 1, false)}
                    >&lt;</button>
                    <button
                      disabled={this.props.userRatingCurrentPage === this.props.userRatingPagesTotal}
                      onClick={() => this.props.fetchUserRatingPage(this.props.userRatingCurrentPage + 1, false)}
                    >&gt;</button>
                    <button
                      disabled={this.props.userRatingCurrentPage === this.props.userRatingPagesTotal}
                      onClick={() => this.props.fetchUserRatingPage(this.props.userRatingPagesTotal, false)}
                    >&gt;&gt;</button>
                  </React.Fragment>
                )
              }
            </div>
          </div>
          <div className="dashboard__tiles-group dashboard__tiles-group--right">
            <div className="dashboard-tile dashboard-tile--race-history">
              <div className="dashboard-tile__header">
                <h3>Race history</h3>
              </div>
              {
                this.props.raceHistory[this.props.raceHistoryCurrentPage] && (
                  <React.Fragment>
                    <table>
                      <thead>
                        <tr>
                          <td>Date</td>
                          <td>Place</td>
                          <td>Car</td>
                          <td>Member amount</td>
                        </tr>
                      </thead>
                      <tbody>
                        {
                          this.props.raceHistory[this.props.raceHistoryCurrentPage].map(race => {
                            const currentUser = race.members.find(m => m.userId === this.props.user.id);
                            return (
                              <tr key={race._id}>
                                <td>{race.date}</td>
                                <td>{currentUser.place}</td>
                                <td>{currentUser.car}</td>
                                <td>{race.membersNeeded}</td>
                              </tr>
                            );
                          })
                        }
                      </tbody>
                      <tfoot>
                        <tr>
                          <td colSpan={3}>Page: {this.props.raceHistoryCurrentPage} / {this.props.raceHistoryPagesTotal}</td>
                        </tr>
                      </tfoot>
                    </table>
                    <button
                      disabled={this.props.raceHistoryCurrentPage === 1}
                      onClick={() => this.props.fetchRaceHistoryPage(1, false)}
                    >&lt;&lt;</button>
                    <button
                      disabled={this.props.raceHistoryCurrentPage === 1}
                      onClick={() => this.props.fetchRaceHistoryPage(this.props.raceHistoryCurrentPage - 1, false)}
                    >&lt;</button>
                    <button
                      disabled={this.props.raceHistoryCurrentPage === this.props.raceHistoryPagesTotal}
                      onClick={() => this.props.fetchRaceHistoryPage(this.props.raceHistoryCurrentPage + 1, false)}
                    >&gt;</button>
                    <button
                      disabled={this.props.raceHistoryCurrentPage === this.props.raceHistoryPagesTotal}
                      onClick={() => this.props.fetchRaceHistoryPage(this.props.raceHistoryPagesTotal, false)}
                    >&gt;&gt;</button>
                  </React.Fragment>
                )
              }
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }

  private onRaceCreate(membersAmount: number) {
    this.props.createRace(membersAmount)
      .then(raceId => this.props.history.push(`${routes.RACE}/${raceId}`))
      .catch(() => alert('Error! Race is not created!'));
  }
}

const mapStateToProps = (state: AppState) => ({
  user: state.user.authUser,
  raceHistory: state.raceHistory.entities,
  isRaceHistoryLoading: state.raceHistory.isLoading,
  raceHistoryCurrentPage: state.raceHistory.currentPage,
  raceHistoryPagesTotal: Math.ceil(state.raceHistory.total / state.raceHistory.perPageAmount),
  userRating: state.userRating.entities,
  isUserRatingLoading: state.userRating.isLoading,
  userRatingCurrentPage: state.userRating.currentPage,
  userRatingPagesTotal: Math.ceil(state.userRating.total / state.userRating.perPageAmount)
});

const mapDispatchToProps = dispatch => ({
  fetchRaceHistoryPage: (page: number, isInitialLoad: boolean) => dispatch(fetchRaceHistoryPage(page, isInitialLoad)),
  fetchUserRatingPage: (page: number, isInitialLoad: boolean) => dispatch(fetchUserRatingPage(page, isInitialLoad)),
  createRace: (membersNeeded: number) => dispatch(createRace(membersNeeded))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardPage);