import './Typer.scss';
import * as React from 'react';

interface Props {
  disabled: boolean;
  initialTypedLength: number;
  text: string;
  onTypedLengthChange: (progress: number) => void;
}

interface State {
  typed: string;
  expected: string;
}

export default class Typer extends React.PureComponent<Props, State> {
  private typerRef: any;

  constructor(props) {
    super(props);

    this.state = {
      typed: this.props.text.slice(0, this.props.initialTypedLength),
      expected: this.props.text.slice(this.props.initialTypedLength)
    };

    this.onKeyPress = this.onKeyPress.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
    this.typerRef = React.createRef();
  }

  public componentDidUpdate(prevProps: Props) {
    if (prevProps.disabled && !this.props.disabled) {
      this.typerRef.current.focus();
    }
  }

  public render() {
    return (
      <div className="text-container">
        <div className="text-container__printed-text-wrapper">
          <div className="text-container__printed-text">{this.state.typed}</div>
        </div>
        <div className="text-container__expected-text">
          <div 
            className="text-container__text-input"
            ref={this.typerRef}
            contentEditable={!this.props.disabled}
            onKeyPress={this.onKeyPress}
            onKeyDown={this.onKeyDown}></div>
          {this.state.expected}
        </div>
      </div>
    );
  }

  private onKeyPress(e: any) {
    e.preventDefault();

    if (this.isAllowedChar(e.charCode)) {
      this.onInputChange(this.state.typed + String.fromCharCode(e.charCode));
    }
  }

  private onKeyDown(e: any) {
    if (!this.isAllowedChar(e.charCode) && e.keyCode === 8) {
      this.onInputChange(this.state.typed.slice(0, this.state.typed.length - 1));
    }
  }

  private isAllowedChar(charCode: number) {
    const char = String.fromCharCode(charCode);
    return /[a-zA-Z0-9-_,:?!"'+=\. ]/.test(char);
  }

  private onInputChange(value: string) {
    let incorrectPos: number = -1;
    for (let pos = 0; pos < value.length; pos++) {
      if (value[pos] != this.props.text[pos]) {
        incorrectPos = pos;
        break;
      }
    }

    let expected: string;
    if (incorrectPos === -1) {
      expected = this.props.text.slice(value.length);
    } else {
      expected = this.props.text.slice(incorrectPos);
    }

    if (this.state.expected != expected) {
      this.props.onTypedLengthChange(value.length);
    }

    this.setState({
      typed: value,
      expected
    });
  }
}