import * as React from 'react';
import { AppState } from './../../state/appState';
import { connect } from 'react-redux';
import CarPicker from '../../components/CarPicker/CarPicker';
import { Car } from './../../models/car';
import { fetchCarsOnSalePage, buyCar } from '../../state/actions/carsOnSale';
import { CarModel } from '../../../../shared/enums/carModel';

interface Props {
  cars: Array<Car>;
  carsOnSale: Array<Car>;
  fetchCarsOnSalePage: (page: number, isInitialLoad: boolean) => Promise<void>;
  buyCar: (model: CarModel) => Promise<void>;
}

class GaragePage extends React.PureComponent<Props> {

  constructor(props: Props) {
    super(props);

    this.buyCar = this.buyCar.bind(this);
  }

  public componentDidMount() {
    this.props.fetchCarsOnSalePage(1, true);
  }

  public render() {
    return (
      <div className="garage">
        <section className="garage__user-cars">
          <h2>My cars</h2>
          <CarPicker
            disabled={true}
            cars={this.props.cars}
            onPick={() => { }}
          />
        </section>
        <section className="garage__cars-on-sale">
          <h2>Cars on sale</h2>
          <CarPicker
            cars={this.props.carsOnSale}
            showPrice={true}
            onPick={this.buyCar}
          />
        </section>
      </div>
    );
  }

  private buyCar(car: Car) {
    this.props.buyCar(car.model)
      .catch(error => {
        alert(error);
      });
  }
}

const mapStateToProps = (state: AppState) => ({
  cars: state.user.authUser.cars,
  carsOnSale: state.carsOnSale.entities[state.carsOnSale.currentPage]
});

const mapDispatchToProps = dispatch => ({
  fetchCarsOnSalePage: (page: number, isInitialLoad: boolean) => dispatch(fetchCarsOnSalePage(page, isInitialLoad)),
  buyCar: (model: CarModel) => dispatch(buyCar(model))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GaragePage);