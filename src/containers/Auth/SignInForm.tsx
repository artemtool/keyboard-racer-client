import * as React from 'react';
import './Auth.scss';

interface Props {
  isLoading: boolean;
  onSubmit: (email: string, password) => void;
}

interface State {
  email: string;
  password: string;
}

export default class SignInForm extends React.PureComponent<Props, State> {

  constructor(props: Props) {
    super(props);

    this.state = {
      email: '',
      password: ''
    }

    this.onSubmit = this.onSubmit.bind(this);
    this.onValueChange = this.onValueChange.bind(this);
  }

  public render() {
    return (
      <form onSubmit={this.onSubmit}>
        <div className="form-field">
          <label htmlFor="email">Email</label>
          <input
            id="email"
            type="email"
            name="email"
            placeholder="Enter your email"
            value={this.state.email}
            onChange={this.onValueChange}
          />
        </div>
        <div className="form-field">
          <label htmlFor="password">Password</label>
          <input
            id="password"
            type="password"
            name="password"
            placeholder="Enter your password"
            value={this.state.password}
            onChange={this.onValueChange}
          />
        </div>
        <button type="submit">Sign in</button>
      </form>
    );
  }

  private onValueChange(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({
      ...this.state,
      [event.target.name]: event.target.value
    });
  }

  private onSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();

    // validation, resseting, etc.

    const {
      email,
      password
    } = this.state;
    this.props.onSubmit(email, password);
  }
}