import * as React from 'react';

interface Props {
  trySignIn: () => Promise<void>;
}

const withAuthentication = (Component: React.ComponentType<any>) =>
  class WithAuthentication extends React.PureComponent<Props> {

    componentDidMount() {
      this.props.trySignIn();
    }

    render() {
      const { trySignIn, ...props } = this.props;
      return (
        <Component {...props} />
      );
    }
  }

export default withAuthentication;