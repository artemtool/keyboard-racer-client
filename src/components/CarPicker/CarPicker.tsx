import * as React from 'react';
import './CarPicker.scss';
import { Car } from './../../models/car';

interface Props {
  cars: Array<Car>;
  onPick: (car: Car) => void;
  disabled?: boolean;
  showPrice?: boolean;
}

export default class CarPicker extends React.PureComponent<Props> {

  public render() {
    const cars = this.props.cars || [];

    return (
      <div className="car-picker__cars">
        {
          cars.map(car =>
            <div
              key={car.model}
              className={`car-picker__car ${this.props.disabled ? 'disabled' : ''}`}
              onClick={() => !this.props.disabled && this.props.onPick(car)}
            >
              <img
                className='car-picker__car-image'
                src={car.imageUrl}
                alt={car.label}
              />
              <span className='car-picker__car-label'>{car.label}</span>
              {
                this.props.showPrice && (
                  <div className='car-price-container'>
                    <img src="/public/assets/images/coins.png" />
                    <span>{car.price}</span>
                  </div>
                )
              }
            </div>
          )
        }
      </div>
    );
  }
}