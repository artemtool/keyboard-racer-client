import { Action } from 'redux';

export interface ListState {
  total: number | null;
  perPageAmount: number;
  isLoading: boolean;
  isDesc: boolean;
  orderBy: string;
}

export interface PaginationListState<T> extends ListState {
  entities: Array<Array<T>>;
  currentPage: number | null;
}

export interface InfiniteScrollListState<T> extends ListState {
  entities: Array<T>;
  canLoadMore: boolean;
  page: number;
}

export interface ListParameters {
  page: number;
  perPageAmount: number;
  isDesc: boolean;
  orderBy: string;
}

export interface RequestAction extends Action {
  isInitialLoad: boolean;
}

export interface ReceiveAction<T> extends Action {
  entities: Array<T>;
  page: number;
  total: number;
}

export interface FailureAction extends Action {
  error: Error;
}

export interface ChangeOrderByAction extends Action {
  orderBy: string;
}

export interface ChangeIsDescAction extends Action {
  isDesc: boolean;
}