import { call, race, takeEvery, put, take } from 'redux-saga/effects'
import { FINISH_RACE_SUCCESS, JOIN_RACE_SUCCESS, updateRaceProgressSuccess, updateCountdownSuccess, UPDATE_RACE_PROGRESS, updateRaceStatusSuccess } from '../actions/race';
import { JoinRaceSuccessAction, UpdateRaceProgressAction } from './../actions/race';
import { socketManager } from './../../index';
import { RaceStatus } from '../../../../shared/enums/raceStatus';
import { User } from '../../models/user';
import { IUser } from './../../../../shared/interfaces/user';
import { updateUserSuccess } from '../actions/user';
import { RaceMember } from './../../models/raceMember';
import { IRaceMember } from './../../../../shared/interfaces/raceMember';

function* onCountdownUpdate() {
  const { raceId, countdown } = yield new Promise((resolve) => {
    socketManager.socket.on('countdownUpdate', (raceId: string, countdown: number) => {
      resolve({
        raceId,
        countdown
      });
    });
  });

  yield put(updateCountdownSuccess(raceId, countdown));
}

function* onUpdateRaceProgress() {
  const action: UpdateRaceProgressAction = yield take(UPDATE_RACE_PROGRESS);
  socketManager.socket.emit('updateRaceProgress', action.raceId, action.typedLength);
}

function* onRaceProgressUpdate() {
  const { raceId, member } = yield new Promise((resolve) => {
    socketManager.socket.on('raceProgressUpdate', (raceId: string, member: IRaceMember) => {
      resolve({
        raceId,
        member: new RaceMember(member)
      });
    });
  });

  yield put(updateRaceProgressSuccess(raceId, member));
}

function* onRaceStatusUpdate() {
  const { raceId, status } = yield new Promise((resolve) => {
    socketManager.socket.on('raceStatusUpdate', (raceId: string, status: RaceStatus) => {
      resolve({
        raceId,
        status
      });
    });
  });

  yield put(updateRaceStatusSuccess(raceId, status));
}

function* onUserUpdate() {
  const user = yield new Promise((resolve) => {
    socketManager.socket.on('userUpdate', (user: IUser) => {
      resolve(new User(user));
    });
  });

  yield put(updateUserSuccess(user));
}

function* connectSocketsIO(action: JoinRaceSuccessAction) {
  socketManager.connect({
    raceId: action.raceId
  });

  while (true) {
    yield race({
      onCountdownUpdate: call(onCountdownUpdate),
      onUpdateRaceProgress: call(onUpdateRaceProgress),
      onRaceProgressUpdate: call(onRaceProgressUpdate),
      onRaceStatusUpdate: call(onRaceStatusUpdate),
      onUserUpdate: call(onUserUpdate)
    });
  }
}

function* disconnectSocketsIO() {
  socketManager.socket.disconnect();
}

function* listenForAuthActions() {
  yield takeEvery(JOIN_RACE_SUCCESS, connectSocketsIO);
  yield takeEvery(FINISH_RACE_SUCCESS, disconnectSocketsIO);
}

export default listenForAuthActions;