import { fork, all } from "redux-saga/effects";

import liveUpdatesSaga from './liveUpdates';

function* rootSaga () {
  yield all([
      fork(liveUpdatesSaga)
  ]);
}

export default rootSaga;