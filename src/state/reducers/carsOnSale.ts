import { initReducer } from './../util/reducerInitializer';
import * as carsOnSale from '../actions/carsOnSale';
import { PaginationListState } from '../util/reduxThunkLists/common/interfaces';
import { addActionHandlers } from '../util/reduxThunkLists/pagination';
import { Car } from './../../models/car';
import { Action } from 'redux';
import { BuyCarSuccessAction } from './../actions/carsOnSale';

export const initialState: PaginationListState<Car> = {
  entities: [],
  total: null,
  currentPage: null,
  perPageAmount: 10,
  isLoading: false,
  isDesc: false,
  orderBy: 'price'
};

const actionHandlers: { [actionType: string]: (state: PaginationListState<Car>, action: Action) => PaginationListState<Car> } = {
  [carsOnSale.BUY_CAR_SUCCESS]: (state: PaginationListState<Car>, action: BuyCarSuccessAction): PaginationListState<Car> => {
    const totalPages = Math.ceil(state.total / state.perPageAmount);
    for (let page = 1; page <= totalPages; page++) {
      const index = state.entities[page].findIndex(c => c.model === action.car.model);
      if (index >= 0) {
        state.entities[page] = state.entities[page].filter(c => c.model !== action.car.model);
        break;
      }
    }
    return {
      ...state,
      entities: [...state.entities]
    };
  }
};

const reducer = initReducer(initialState, actionHandlers);
const carsOnSaleReducer = addActionHandlers<Car>(reducer, carsOnSale.ACTIONS_SUFFIX);
export default carsOnSaleReducer;