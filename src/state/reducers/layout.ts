import { initReducer } from './../util/reducerInitializer';
import { Action } from 'redux';
import * as layout from "../actions/layout";

export interface State {
  totalLoaders: number;
}

export const initialState: State = {
  totalLoaders: 0
};

const actionHandlers: { [actionType: string]: (state: State, action: Action) => State } = {
  [layout.SHOW_LOADER]: (state: State): State => {
    return {
      ...state,
      totalLoaders: state.totalLoaders + 1
    };
  },
  [layout.HIDE_LOADER]: (state: State): State => {
    return {
      ...state,
      totalLoaders: state.totalLoaders - 1
    };
  }
};

const layoutReducer = initReducer(initialState, actionHandlers);
export default layoutReducer;