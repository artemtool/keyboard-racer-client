import { initReducer } from './../util/reducerInitializer';
import { IRaceHistoryItem } from './../../../../shared/interfaces/raceHistoryItem';
import { ACTIONS_SUFFIX } from '../actions/raceHistory';
import { PaginationListState } from '../util/reduxThunkLists/common/interfaces';
import { addActionHandlers } from '../util/reduxThunkLists/pagination';

export const initialState: PaginationListState<IRaceHistoryItem> = {
  entities: [],
  total: null,
  currentPage: null,
  perPageAmount: 10,
  isLoading: false,
  isDesc: false,
  orderBy: 'date'
};

const reducer = initReducer(initialState);
const raceHistoryReducer = addActionHandlers<IRaceHistoryItem>(reducer, ACTIONS_SUFFIX);
export default raceHistoryReducer;

