import { initReducer } from './../util/reducerInitializer';
import { ACTIONS_SUFFIX } from '../actions/userRating';
import { PaginationListState } from '../util/reduxThunkLists/common/interfaces';
import { addActionHandlers } from '../util/reduxThunkLists/pagination';
import { User } from './../../models/user';

export const initialState: PaginationListState<User> = {
  entities: [],
  total: null,
  currentPage: null,
  perPageAmount: 10,
  isLoading: false,
  isDesc: true,
  orderBy: 'rating'
};

const reducer = initReducer(initialState);
const userRatingReducer = addActionHandlers<User>(reducer, ACTIONS_SUFFIX);
export default userRatingReducer;

