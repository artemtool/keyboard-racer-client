import { initReducer } from './../util/reducerInitializer';
import { Action } from 'redux';
import * as race from "../actions/race";
import { RaceStatus } from '../../../../shared/enums/raceStatus';
import { IArticle } from './../../../../shared/interfaces/article';
import { RaceMember } from './../../models/raceMember';

export interface State {
  raceId: string;
  startedDate: Date;
  members: Array<RaceMember>;
  countdown: number;
  status: RaceStatus;
  article: IArticle;
}

export const initialState: State = {
  raceId: null,
  startedDate: null,
  members: [],
  countdown: null,
  status: null,
  article: null
};

const actionHandlers: { [actionType: string]: (state: State, action: Action) => State } = {
  [race.CREATE_RACE_SUCCESS]: (state: State, action: race.CreateRaceSuccessAction): State => {
    return {
      ...state,
      raceId: action.race.id,
      startedDate: new Date(action.race.date),
      status: action.race.status,
      article: action.race.article
    }
  },
  [race.JOIN_RACE_SUCCESS]: (state: State, action: race.JoinRaceSuccessAction): State => {
    return {
      ...state,
      raceId: action.raceId,
      startedDate: new Date(action.race.date),
      members: action.race.members,
      status: action.race.status,
      article: action.race.article
    }
  },
  [race.UPDATE_RACE_PROGRESS_SUCCESS]: (state: State, action: race.UpdateRaceProgressSuccessAction): State => {
    if (state.raceId === action.raceId) {
      const raceMemberIndex = state.members.findIndex(m => m.userId === action.raceMember.userId);
      const raceMembers = [...state.members];

      if (raceMemberIndex >= 0) {
        raceMembers.splice(raceMemberIndex, 1, action.raceMember);
      } else {
        raceMembers.push(action.raceMember);
      }

      return {
        ...state,
        members: raceMembers
      }
    } else {
      return state;
    }
  },
  [race.UPDATE_COUNTDOWN_SUCCESS]: (state: State, action: race.UpdateCountdownAction): State => {
    if (state.raceId === action.raceId) {
      return {
        ...state,
        countdown: action.countdown
      }
    } else {
      return state;
    }
  },
  [race.UPDATE_RACE_STATUS_SUCCESS]: (state: State, action: race.UpdateRaceStatusSuccess): State => {
    if (state.raceId === action.raceId) {
      return {
        ...state,
        status: action.status
      }
    } else {
      return state;
    }
  }
};

const userReducer = initReducer(initialState, actionHandlers);
export default userReducer;