import { apiManager } from '../..';
import { AuthorizationType } from '../../common/apiManager';
import { AppState } from './../appState';
import { createChangeIsDescAction, createChangeOrderByAction } from '../util/reduxThunkLists/common/actions';
import { ListParameters, ChangeOrderByAction, ChangeIsDescAction } from '../util/reduxThunkLists/common/interfaces';
import { createThunk } from './../util/reduxThunkLists/pagination';
import { Car } from './../../models/car';
import { CarModel } from '../../../../shared/enums/carModel';
import { Action } from 'redux';
import { ICar } from './../../../../shared/interfaces/car';

export const ACTIONS_SUFFIX = 'CARS_ON_SALE';

export const BUY_CAR_SUCCESS = 'BUY_CAR_SUCCESS';

export interface BuyCarSuccessAction extends Action {
  car: ICar;
}

export function buyCarSuccess(car: ICar): BuyCarSuccessAction {
  return {
    type: BUY_CAR_SUCCESS,
    car
  };
}

export function fetchCarsOnSalePage(page: number, isInitialLoad: boolean = false) {
  const headers = {
    'Content-type': 'application/json'
  };

  const featureStateSelector = (state: AppState) => state.carsOnSale;
  const dataSelector = (parameters: ListParameters, state: AppState) => {
    return apiManager.fetch({
      authorizationType: AuthorizationType.Mandatory,
      isGlobalLoaderShown: false
    }, 'http://localhost:3003/car/list/onsale', {
        method: 'POST',
        headers,
        body: JSON.stringify(parameters)
      });
  };
  const dataMapper = (result) => {
    return {
      total: result.total,
      entities: result.entities.map(e => new Car(e))
    };
  };

  return createThunk<AppState, Car>(
    featureStateSelector,
    dataSelector,
    isInitialLoad,
    page,
    ACTIONS_SUFFIX,
    dataMapper
  );
}

export function changeOrderBy(orderBy: string): ChangeOrderByAction {
  return createChangeOrderByAction(ACTIONS_SUFFIX, orderBy);
}

export function changeIsDesc(isDesc: boolean): ChangeIsDescAction {
  return createChangeIsDescAction(ACTIONS_SUFFIX, isDesc);
}

export function buyCar(carModel: CarModel) {
  return dispatch => {
    return apiManager.fetch({
      authorizationType: AuthorizationType.Mandatory
    }, `http://localhost:3003/car/buy/${carModel}`, {
        method: 'POST'
      })
      .then(res => {
        if (res.status === 400) {
          return res
            .text()
            .then(text => {
              throw new Error(text);
            });
        } else {
          return res
            .json()
            .then(car => dispatch(buyCarSuccess(car)));
        }
      });
  };
}