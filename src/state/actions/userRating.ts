import { apiManager } from '../..';
import { AuthorizationType } from '../../common/apiManager';
import { AppState } from './../appState';
import { createChangeIsDescAction, createChangeOrderByAction } from '../util/reduxThunkLists/common/actions';
import { ListParameters, ChangeOrderByAction, ChangeIsDescAction } from '../util/reduxThunkLists/common/interfaces';
import { createThunk } from './../util/reduxThunkLists/pagination';
import { User } from './../../models/user';

export const ACTIONS_SUFFIX = 'USER_RATING';

export function fetchUserRatingPage(page: number, isInitialLoad: boolean = false) {
  const headers = {
    'Content-type': 'application/json'
  };

  const featureStateSelector = (state: AppState) => state.userRating;
  const dataSelector = (parameters: ListParameters, state: AppState) => {
    return apiManager.fetch({
      authorizationType: AuthorizationType.Mandatory,
      isGlobalLoaderShown: false
    }, 'http://localhost:3003/user/rating', {
        method: 'POST',
        headers,
        body: JSON.stringify(parameters)
      });
  };
  const dataMapper = (result) => {
    return {
      total: result.total,
      entities: result.entities.map(e => new User(e))
    };
  };

  return createThunk<AppState, User>(
    featureStateSelector,
    dataSelector,
    isInitialLoad,
    page,
    ACTIONS_SUFFIX,
    dataMapper
  );
}

export function changeOrderBy(orderBy: string): ChangeOrderByAction {
  return createChangeOrderByAction(ACTIONS_SUFFIX, orderBy);
}

export function changeIsDesc(isDesc: boolean): ChangeIsDescAction {
  return createChangeIsDescAction(ACTIONS_SUFFIX, isDesc);
}