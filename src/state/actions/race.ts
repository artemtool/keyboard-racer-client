import { Dispatch, Action } from "redux";
import { Race } from './../../models/race';
import { AppState } from './../appState';
import { IRace } from './../../../../shared/interfaces/race';
import { User } from './../../models/user';
import { CarModel } from "../../../../shared/enums/carModel";
import { apiManager } from './../../index';
import { AuthorizationType } from "../../common/apiManager";
import { RaceStatus } from "../../../../shared/enums/raceStatus";
import { UpdateRaceStatusSuccess } from './race';
import { RaceMember } from './../../models/raceMember';

export const CREATE_RACE_SUCCESS = 'CREATE_RACE_SUCCESS';
export const JOIN_RACE_SUCCESS = 'JOIN_RACE_SUCCESS';
export const FINISH_RACE_SUCCESS = 'FINISH_RACE_SUCCESS';
export const UPDATE_RACE_PROGRESS = 'UPDATE_RACE_PROGRESS';
export const UPDATE_RACE_PROGRESS_SUCCESS = 'UPDATE_RACE_PROGRESS_SUCCESS';
export const UPDATE_COUNTDOWN_SUCCESS = 'UPDATE_COUNTDOWN_SUCCESS';
export const UPDATE_RACE_STATUS_SUCCESS = 'UPDATE_RACE_STATUS_SUCCESS';

export interface CreateRaceSuccessAction extends Action {
  race: Race;
  user: User;
}

export interface JoinRaceSuccessAction extends Action {
  raceId: string;
  race: Race;
}

export interface UpdateRaceProgressAction extends Action {
  raceId: string;
  typedLength: number;
}

export interface UpdateRaceProgressSuccessAction extends Action {
  raceId: string;
  raceMember: RaceMember;
}

export interface UpdateCountdownAction extends Action {
  raceId: string;
  countdown: number;
}

export interface UpdateRaceStatusSuccess extends Action {
  raceId: string;
  status: RaceStatus;
}

export function createRaceSuccess(race: Race, user: User): CreateRaceSuccessAction {
  return {
    type: CREATE_RACE_SUCCESS,
    race,
    user
  }
}

export function joinRaceSuccess(raceId: string, race: Race): JoinRaceSuccessAction {
  return {
    type: JOIN_RACE_SUCCESS,
    raceId,
    race
  }
}

export function updateRaceProgress(raceId: string, typedLength: number): UpdateRaceProgressAction {
  return {
    type: UPDATE_RACE_PROGRESS,
    raceId,
    typedLength
  }
}

export function updateRaceProgressSuccess(raceId: string, raceMember: RaceMember): UpdateRaceProgressSuccessAction {
  return {
    type: UPDATE_RACE_PROGRESS_SUCCESS,
    raceId,
    raceMember
  }
}

export function updateCountdownSuccess(raceId: string, countdown: number): UpdateCountdownAction {
  return {
    type: UPDATE_COUNTDOWN_SUCCESS,
    raceId,
    countdown
  }
}

export function updateRaceStatusSuccess(raceId: string, status: RaceStatus): UpdateRaceStatusSuccess {
  return {
    type: UPDATE_RACE_STATUS_SUCCESS,
    raceId,
    status
  }
}

export function createRace(membersNeeded: number) {
  return (dispatch: Dispatch, getState: () => AppState) => {
    const headers = {
      'Content-Type': 'application/json'
    };

    return apiManager.fetch({
      authorizationType: AuthorizationType.Mandatory
    }, `http://localhost:3003/race/create`, {
        method: 'POST',
        headers,
        body: JSON.stringify({ membersNeeded })
      })
      .then(response => response.json())
      .then((race: IRace) => {
        const state = getState();
        const mappedModel = new Race(race);
        dispatch(createRaceSuccess(mappedModel, state.user.authUser));
        return race._id;
      })
      .catch(error => {
        throw error;
      })
  }
}

export function joinRace(raceId: string, car: CarModel) {
  return (dispatch: Dispatch) => {
    const headers = {
      'Content-Type': 'application/json'
    };

    return apiManager.fetch({
      authorizationType: AuthorizationType.Mandatory
    }, `http://localhost:3003/race/join/${raceId}`, {
        method: 'POST',
        headers,
        body: JSON.stringify({ car })
      })
      .then(response => {
        if (response.status === 404) {
          throw new Error("Race not found");
        } else {
          return response.json();
        }
      })
      .then((race: IRace) => {
        dispatch(joinRaceSuccess(raceId, new Race(race)));
      })
      .catch(error => {
        throw error;
      })
  }
}