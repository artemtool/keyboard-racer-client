export const HOME = '/';
export const AUTH = '/auth';
export const SIGN_IN = AUTH + '/signin';
export const SIGN_UP = AUTH + '/signup';
export const RACE = '/race';
export const GARAGE = '/garage';
export const DASHBOARD = '/dashboard';